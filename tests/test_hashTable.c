//
// Created by hc on 17.01.2023.
//

#define CTEST_MAIN
#define CTEST_COLOR_OK

#include "../src/hashTable.h"
#include "ctest.h"

#define CAPACITY 128

CTEST(calculateHash, checkHashFunction1) {
    char input[] = "test";
    unsigned int expected = ('t' + 'e' + 's' + 't') % CAPACITY;
    unsigned int result = calculateHash(input);
    ASSERT_EQUAL(expected, result);
}

CTEST(calculateHash, checkHashFunction2) {
    char input[] = "";
    unsigned int expected = 0 % CAPACITY;
    unsigned int result = calculateHash(input);
    ASSERT_EQUAL(expected, result);
}

CTEST(createTable, returnsValidHashTable) {
    int size = 100;
    hashTable* pHashTable = createTable(size);
    ASSERT_NOT_NULL(pHashTable);
    ASSERT_EQUAL(pHashTable->size, size);
    ASSERT_EQUAL(pHashTable->itemCount, 0);
    ASSERT_NOT_NULL(pHashTable->items);
    for (int i = 0; i < pHashTable->size; i++)
        ASSERT_NULL(pHashTable->items[i]);

    ASSERT_NOT_NULL(pHashTable->collisionList);
    free(pHashTable);
}

CTEST(htInsert, insertsItemIntoEmptySlot) {
    hashTable* pHashTable = createTable(100);
    htInsert(pHashTable, "key", "value");

    unsigned int index = calculateHash("key");
    hashTableItem* item = pHashTable->items[index];

    ASSERT_NOT_NULL(item);
    ASSERT_STR(item->key, "key");
    ASSERT_STR(item->value, "value");
    free(pHashTable);
}

CTEST(htInsert, handleCollision) {
    hashTable* pHashTable = createTable(2);
    htInsert(pHashTable, "key1", "value1");
    htInsert(pHashTable, "key2", "value2");

    unsigned int index = calculateHash("key1");
    hashTableItem* item = pHashTable->items[index];

    ASSERT_NOT_NULL(item);
    ASSERT_STR(item->key, "key1");
    ASSERT_STR(item->value, "value1");

    unsigned int index2 = calculateHash("key2");
    hashTableItem* item2 = pHashTable->items[index2];
    ASSERT_NOT_NULL(item2);
    ASSERT_STR(item2->key, "key2");
    ASSERT_STR(item2->value, "value2");
    free(pHashTable);
}

CTEST(htInsert, itemCount) {
    hashTable* pHashTable = createTable(2);
    htInsert(pHashTable, "key1", "value1");
    htInsert(pHashTable, "key2", "value2");

    int itemCount = pHashTable->itemCount;
    //htInsert(pHashTable, "key3", "value3"); this causes the output of "hash table is full" and I don't know how to catch that yet

    ASSERT_EQUAL(pHashTable->itemCount, itemCount);
    free(pHashTable);
}

int main(int argc, const char *argv[])
{
    int result = ctest_main(argc, argv);

    return result;
}