//
// Created by hc on 19.01.2023.
//

#define CTEST_MAIN
#define CTEST_COLOR_OK

#include "../src/hashTable.h"
#include "ctest.h"

#define CAPACITY 128

CTEST(insertItemOntoLinkedList, creates_new_list_when_null) {
    hashTableItem* item = createItem("key", "value");
    linkedList* list = insertItemOntoLinkedLIst(NULL, item);

    ASSERT_NOT_NULL(list);
    ASSERT_NOT_NULL(list->hashTableItem);
    ASSERT_STR(list->hashTableItem->key, "key");
    ASSERT_STR(list->hashTableItem->value, "value");
    ASSERT_NULL(list->nextItem);
    free(item);
    free(list);
}

CTEST(insertItemOntoLinkedList, appends_to_existing_list) {
    hashTableItem* item1 = createItem("key1", "value1");
    hashTableItem* item2 = createItem("key2", "value2");
    linkedList* list = insertItemOntoLinkedLIst(NULL, item1);
    list = insertItemOntoLinkedLIst(list, item2);

    ASSERT_NOT_NULL(list);
    ASSERT_NOT_NULL(list->hashTableItem);
    ASSERT_STR(list->hashTableItem->key, "key1");
    ASSERT_STR(list->hashTableItem->value, "value1");
    ASSERT_NOT_NULL(list->nextItem);
    ASSERT_NOT_NULL(list->nextItem->hashTableItem);
    ASSERT_STR(list->nextItem->hashTableItem->key, "key2");
    ASSERT_STR(list->nextItem->hashTableItem->value, "value2");
    free(item1);
    free(item2);
    free(list);
}

int main(int argc, const char *argv[])
{
    int result = ctest_main(argc, argv);

    return result;
}