//
//  main.c
//  HashTable
//
//  Created by hc on 29.12.2022.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "src/linkedList.h"
#include "src/hashTable.h"

#define CAPACITY 128

#define MAX_KEY_LENGTH 6
#define MAX_VALUE_LENGTH 32
#define NUM_ITEMS 32

int main()
{
    hashTable* ht = createTable(CAPACITY);

    srand((unsigned) time(NULL));

    printf("Begin\n");

    clock_t start, end;
    double cpu_time_used;
    start = clock();

    for (int i = 0; i < NUM_ITEMS; i++) {
        char key[MAX_KEY_LENGTH + 1];
        char value[MAX_VALUE_LENGTH + 1];
        for (int keyChar = 0; keyChar < MAX_KEY_LENGTH; keyChar++) {
            key[keyChar] = 'A' + rand() % 26;
        }
        key[MAX_KEY_LENGTH] = '\0';

        for (int valueChar = 0; valueChar < MAX_VALUE_LENGTH; valueChar++) {
            value[valueChar] = 'A' + rand() % 26;
        }
        value[MAX_VALUE_LENGTH] = '\0';

        htInsert(ht, key, value);
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time taken: %f\n", cpu_time_used);

    printHT(ht);
    printHTItemCount(ht);

    freeTable(ht);

    printf("End\n");

    return 0;
}
