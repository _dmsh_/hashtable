//
//  hashtable.h
//  HashTable
//
//  Created by hc on 29.12.2022.
//

#ifndef hashTable_h
#define hashTable_h

#include "linkedList.h"

typedef struct hashTableItem hashTableItem;
typedef struct hashTable hashTable;
typedef struct linkedList linkedList;

struct hashTableItem {
    char* key;
    char* value;
};

struct hashTable {
    struct hashTableItem** items;
    linkedList** collisionList;

    int itemCount;
    int size;
};

unsigned int calculateHash(char* input);

hashTableItem* createItem(char* key, char* value);
hashTable* createTable(int size);

void htInsert(hashTable* pHashTable, char* key, char* value);
char* htSearch(hashTable* pHashTable, char* key);
void freeTable(hashTable* pHashTable);
void printHT(hashTable* pHashTable);
void printHTItemCount(hashTable* pHashTable);
linkedList** createCollisionList(hashTable* pHashTable);
void freeCollisionList(hashTable* pHashTable);
void htDelete(hashTable* pHashTable, char* key);

#endif /* hashTable_h */