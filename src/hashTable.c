//
//  hashtable.c
//  HashTable
//
//  Created by hc on 29.12.2022.
//

#include "hashTable.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define CAPACITY 128

//local functions
void startNewList(hashTable *pHashTable, unsigned long index, hashTableItem *item);
void handleCollision(hashTable* pHashTable, unsigned long index, hashTableItem* item);

unsigned int calculateHash(char* input) {
    unsigned int hash = 0;
    char* p;

    for (p = input; *p != '\0'; p++) {
        hash += *p;
    }

    return hash % CAPACITY;
}

hashTableItem* createItem(char* key, char* value) {
    hashTableItem* item = (hashTableItem*) malloc (sizeof(hashTableItem));
    item->key = (char*) malloc (sizeof(key) + 1);
    item->value = (char*) malloc (strlen(value) + 1);
    
    strcpy(item->key, key);
    strcpy(item->value, value);

    return item;
}

hashTable* createTable(int size) {
    hashTable* pHashTable = (hashTable*) malloc (sizeof(hashTable));
    pHashTable->size = size;
    pHashTable->itemCount = 0;
    pHashTable->items = (hashTableItem**) calloc (pHashTable->size, sizeof(hashTableItem*));
    for (int i = 0; i < pHashTable->size; i++)
        pHashTable->items[i] = NULL;

    pHashTable->collisionList = createCollisionList(pHashTable);

    return pHashTable;
}

void freeItem(hashTableItem* item) {
    free(item->key);
    free(item->value);
    free(item);
}

void freeTable(hashTable* pHashTable) {
    for (int i = 0; i < pHashTable->size; i++) {
        hashTableItem* item = pHashTable->items[i];
        if (item != NULL)
            freeItem(item);
    }

    freeCollisionList(pHashTable);
    free(pHashTable->items);
    free(pHashTable);
}

void htInsert(hashTable* pHashTable, char* key, char* value) {
    hashTableItem* item = createItem(key, value);

    unsigned int index = calculateHash(key);
    hashTableItem* currentItem = pHashTable->items[index];
    if (currentItem == NULL) {
        if (pHashTable->itemCount == pHashTable->size) {
            printf("Error: Hash Table is full\n");
            freeItem(item);
        }

        pHashTable->items[index] = item;
        pHashTable->itemCount++;
    } else {
        handleCollision(pHashTable, index, item);
    }
}

char* htSearch(hashTable* pHashTable, char* key) {
    unsigned int index = calculateHash(key);
    hashTableItem* item = pHashTable->items[index];
    linkedList* head = pHashTable->collisionList[index];

    while (item != NULL) {
        if (strcmp(item->key, key) == 0)
            return item->value;
        if (head == NULL)
            return NULL;
        item = head->hashTableItem;
        head = head->nextItem;
    }

    return NULL;
}

void printHT(hashTable* pHashTable) {
    for (int i = 0; i < pHashTable->size; i++) {
        if (pHashTable->items[i]) {
            printf("Index: %d, Key: %s, Value: %s\n", i, pHashTable->items[i]->key, pHashTable->items[i]->value);
        }
    }
}

void printHTItemCount(hashTable* pHashTable) {
    printf("Hash pHashTable item count: %i\n", pHashTable->itemCount);
}

linkedList** createCollisionList(hashTable* pHashTable) {
    linkedList** collisionList = (linkedList**) calloc (pHashTable->size, sizeof(linkedList*));
    for (int i = 0; i < pHashTable->size; i++)
        collisionList[i] = NULL;

    return collisionList;
}

void freeCollisionList(hashTable* pHashTable) {
    linkedList** buckets = pHashTable->collisionList;
    for (int i=0; i < pHashTable->size; i++)
        freeLinkedList(buckets[i]);
    free(buckets);
}

void handleCollision(hashTable* pHashTable, unsigned long index, hashTableItem* item) {
    linkedList* head = pHashTable->collisionList[index];

    if (head == NULL)
        startNewList(pHashTable, index, item);
    else
        pHashTable->collisionList[index] = insertItemOntoLinkedLIst(head, item);
}

void startNewList(hashTable *pHashTable, unsigned long index, hashTableItem *item) {
    linkedList* head = allocateList();
    head->hashTableItem = item;
    pHashTable->collisionList[index] = head;
}

void htDelete(hashTable* pHashTable, char* key) {
    unsigned int index = calculateHash(key);
    hashTableItem * pHashTableItem = pHashTable->items[index];
    linkedList* head = pHashTable->collisionList[index];

    if (pHashTableItem != NULL) {
        if (head == NULL && strcmp(pHashTableItem->key, key) == 0) {
            pHashTable->items[index] = NULL;
            freeItem(pHashTableItem);
            pHashTable->itemCount--;
        } else if (head != NULL) {
            if (strcmp(pHashTableItem->key, key) == 0) {
                freeItem(pHashTableItem);
                linkedList *node = head;
                head = head->nextItem;
                node->nextItem = NULL;
                pHashTable->items[index] = createItem(node->hashTableItem->key, node->hashTableItem->value);
                freeLinkedList(node);
                pHashTable->collisionList[index] = head;
            }

            linkedList *curr = head;
            linkedList *prev = NULL;
            while (curr) {
                if (strcmp(curr->hashTableItem->key, key) == 0) {
                    if (prev == NULL) {
                        freeLinkedList(head);
                        pHashTable->collisionList[index] = NULL;
                    } else {
                        prev->nextItem = curr->nextItem;
                        curr->nextItem = NULL;
                        freeLinkedList(curr);
                        pHashTable->collisionList[index] = head;
                    }
                }

                curr = curr->nextItem;
                prev = curr;
            }
        }
    }
}