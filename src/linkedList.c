//
//  linkedList.c
//  HashTable
//
//  Created by hc on 13.01.2023.
//

#include "linkedList.h"

#include <stdlib.h>

linkedList* insertItemOntoLinkedLIst(linkedList* list, hashTableItem* item) {
    if (!list) {
        linkedList* listHead = allocateList();
        listHead->hashTableItem = item;
        listHead->nextItem = NULL;
        list = listHead;
        list->lastItem = listHead;
    } else {
        linkedList* node =  allocateList();
        node->hashTableItem = item;
        node->nextItem = NULL;
        list->lastItem->nextItem = node;
        list->lastItem = node;
    }

    return list;
}

linkedList* allocateList () {
    linkedList* list = (linkedList*) malloc (sizeof(linkedList));

    return list;
}

hashTableItem* pop(linkedList* list) {
    if (!list)
        return NULL;
    if (!list->nextItem)
        return NULL;

    linkedList* node = list->nextItem;
    linkedList* temp = list;
    temp->nextItem = NULL;
    list = node;

    return temp->hashTableItem;
}

void freeLinkedList(linkedList* list) {
    linkedList* temp;
    while (list) {
        temp = list;
        list = list->nextItem;
        free(temp->hashTableItem->key);
        free(temp->hashTableItem->value);
        free(temp->hashTableItem);
        free(temp);
    }
}