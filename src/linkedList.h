//
//  linkedList.h
//  HashTable
//
//  Created by hc on 13.01.2023.
//

#ifndef linkedList_h
#define linkedList_h

#include "hashTable.h"

typedef struct linkedList linkedList;
typedef struct hashTableItem hashTableItem;
typedef struct hashTable hashTable;

struct linkedList {
    hashTableItem* hashTableItem;
    struct linkedList* nextItem;
    struct linkedList* lastItem;
};

linkedList* insertItemOntoLinkedLIst(linkedList* list, hashTableItem* item);
linkedList* allocateList ();
hashTableItem* pop(linkedList* list);
void freeLinkedList(linkedList* list);

#endif /* linkedList_h */